#!/usr/bin/env fish
for i in all-images/*.png
	set name (string split '/' $i)[2]
	if test (count *collection/$name) = 0
		echo $i
	end
end
