!! downscaled images for faster loadtime, click to get to the full resolution image !!

[![forgotten-world-angkor-wat](../icons/forgotten-world-angkor-wat.webp)](../all-images/forgotten-world-angkor-wat.png)
[![forgotten-world-capitol](../icons/forgotten-world-capitol.webp)](../all-images/forgotten-world-capitol.png)
[![forgotten-world-colosseum](../icons/forgotten-world-colosseum.webp)](../all-images/forgotten-world-colosseum.png)
[![forgotten-world-gizeh](../icons/forgotten-world-gizeh.webp)](../all-images/forgotten-world-gizeh.png)
[![forgotten-world-hungarian-parliament](../icons/forgotten-world-hungarian-parliament.webp)](../all-images/forgotten-world-hungarian-parliament.png)
[![forgotten-world-saint-basils](../icons/forgotten-world-saint-basils.webp)](../all-images/forgotten-world-saint-basils.png)
[![forgotten-world-schwanstein](../icons/forgotten-world-schwanstein.webp)](../all-images/forgotten-world-schwanstein.png)
[![forgotten-world-taj-mahal](../icons/forgotten-world-taj-mahal.webp)](../all-images/forgotten-world-taj-mahal.png)
