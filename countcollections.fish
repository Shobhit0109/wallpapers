#!/usr/bin/env fish

for i in *collection/
	printf "$(ls $i | wc -l)\t$i\n" | rev | cut -c 2- | rev
end
