# wallpaper repository of quantenzitrone

## examples

![example1](all-images/astronaut-holding-space-jellyfish.png)
![example2](all-images/carcharoth.png)
![example3](all-images/thefatrat-mayday.png)
![example4](all-images/enjoing-the-sunset-on-a-cliff.png)
![example5](all-images/idea-of-you.png)
![example6](all-images/foggy-mountain-woods.png)
![example7](all-images/mountains.png)
![example8](all-images/red-waterfall-solar-eclipse.png)
![example9](all-images/scratcherpen-the-witcher-misty-dark-forest.png)

## License / Copyright

I have no rights to these pictures, as they are pictures I gathered over time online from various sources.

If you are the copyright holder of one or more of these pictures and want them taken down, please contact me via either an issue on this repository, or via Matrix on @quantenzitrone:matrix.org. You may also find me on various other platforms on the internet.
